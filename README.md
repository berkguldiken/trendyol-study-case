# Trendyol Case Study
## How to run the job
### _Pulling and Starting The Docker Compose Components_
- First docker engine and docker compose needs to be installed on the machine _(Please refer to [Docker](https://docs.docker.com/get-docker/))_
_Disclaimer: If you are in linux please give chmod 777 permission to the ./docker-volume-test folder. Otherwise flink will throw and error which it cannot be writable to given folder and will write the output files to the container itself. You can check the content of the output in docker container with "docker exec -it {container_id OR container_name} bash" command_
- After installing start the docker containers using docker-compose:
```
 $ sudo docker-compose up -d (for linux)
 docker-compose up -d (for windows)
```
- After that use below command for checking the containers are running and note the jobmanager container name (will be used later)
```
 $ sudo docker ps (for linux)
 docker ps (for windows)
```
- You will see similar code after above code command executed
```
CONTAINER ID   IMAGE                     COMMAND                  CREATED          STATUS          PORTS                        NAMES
76407fe68a00   flink:1.12.0-scala_2.11   "/docker-entrypoint.…"   43 minutes ago   Up 39 minutes   6123/tcp, 8081/tcp           trendyolstudy_taskmanager_1
e2c93754eb0d   flink:1.12.0-scala_2.11   "/docker-entrypoint.…"   43 minutes ago   Up 39 minutes   6123/tcp, 0.0.0.0:8081->8081/tcp         trendyolstudy_jobmanager_1
```
- open the [localhost:8081](localhost:8081). Then you can see the flink container is running and will see the future task on the web ui

- Now is the time for executing the job with flink and docker container. Below command needs to be changes a little(first row is for linux based, second row is for windows users)
```
$ sudo docker exec -it -u 0 {put_jobmanager_container_name OR container_id} flink run -d -c trendyol.StudyCase /opt/flink/usrlib/job.jar --input /opt/flink/usrlib/case.csv --output-folder /opt/flink/usrlib/

docker exec -it -u 0 {put_jobmanager_container_name OR container_id} flink run -d -c trendyol.StudyCase /opt/flink/usrlib/job.jar --input /opt/flink/usrlib/case.csv --output-folder /opt/flink/usrlib/
```
example command:
```
$ sudo docker exec -it trendyolstudy_jobmanager_1 flink run -d -c trendyol.StudyCase /opt/flink/usrlib/job.jar --input /opt/flink/usrlib/case.csv --output-folder /opt/flink/usrlib/
```
### _After Executing The Task_

- Now everything should be done! You can test the output files on the ./docker-volume-test
- Two things that you need to keep in mind:
-- first if you want to run the job again please delete the previous created files (question.txt files)
-- If flink gives an error first you can check ./docker-volume-test folder has the permission (note: windows does not need these permission as far as I know) with chmod 777 

- After you are done with the Docker Containers you can kill them using:
```
$ sudo docker-compose kill
```
