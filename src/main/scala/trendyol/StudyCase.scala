package trendyol

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.common.operators.Order
import org.apache.flink.api.scala._
import org.apache.flink.util.Collector
import org.apache.flink.api.java.utils.ParameterTool

//import org.apache.flink.table.api

/**
 * Implements the "WordCount" program that computes a simple word occurrence histogram
 * over some sample data
 *
 * This example shows how to:
 *
 *   - write a simple Flink program.
 *   - use Tuple data types.
 *   - write and use user-defined functions.
 */
object StudyCase {
  def main(args: Array[String]) {


    val params: ParameterTool = ParameterTool.fromArgs(args)
    // set up the execution environment
    val env = ExecutionEnvironment.getExecutionEnvironment
    env.getConfig.setGlobalJobParameters(params)
    if(!params.has("input") && !params.has("output-folder")) {
      println("You need to specify the input file and output folder paths")
      sys.exit()
    }
    case class trendyol_data(val date:Int,productid:Int,eventname:String,userid:Int)

    //read main file
    val text = env.readCsvFile[trendyol_data](ignoreFirstLine=true,filePath =params.get("input") ,fieldDelimiter = "|")

    val question1 = text.filter{_.eventname=="view"}
      .map{item =>(item.productid,1)}
      .groupBy(0)
      .sum(1)
      .writeAsCsv(filePath=params.get("output-folder")+ "question1.txt",fieldDelimiter = "|")
      .setParallelism(1)

    val question2 = text.map{item =>(item.eventname,1)}
      .groupBy(0)
      .sum(1)
      .writeAsCsv(filePath=params.get("output-folder")+ "question2.txt",fieldDelimiter = "|")
      .setParallelism(1)


    val question3 = text
      .sortPartition(3,Order.ASCENDING)
      .groupBy(_.userid)
      .reduceGroup {
        (in, out: Collector[(Int,Int)]) =>
          var counts: Array[Int] = Array(0,0,0,0)
          var id: Int = 0
          for (t <- in) {
            id = t.userid
            if (t.eventname == "view") counts(0) = counts(0) + 1
            if (t.eventname == "add") counts(1) = counts(1) + 1
            if (t.eventname == "remove") counts(2) = counts(2) + 1
            if (t.eventname == "click") counts(3) = counts(3) + 1
          }

          if (counts(0) !=0 && counts(1) !=0 && counts(2) !=0 && counts(3) !=0)
            out.collect(id, counts.sum)
      }
      .setParallelism(1)
      .sortPartition(1, Order.DESCENDING)
      .setParallelism(1)
      .first(5)
      .map(item => new Tuple1(item._1))
      .writeAsCsv(filePath=params.get("output-folder") + "question3.txt",fieldDelimiter = "|")
      .setParallelism(1)


    val question4 = text
      .filter{_.userid == 47}
      .map( item => (item.eventname,1))
      .groupBy(0)
      .sum(1)
      .writeAsCsv(filePath=params.get("output-folder")+ "question4.txt",fieldDelimiter = "|")
      .setParallelism(1)


    val question5 = text
      .filter (item => item.eventname== "view" && item.userid ==47 )
      .map (item => Tuple1(item.productid))
      .writeAsCsv(filePath=params.get("output-folder")+ "question5.txt",fieldDelimiter = "|")
      .setParallelism(1)

      //env.execute("Flink Scala Trendyol")

  }
}

